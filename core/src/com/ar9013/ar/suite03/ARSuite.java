package com.ar9013.ar.suite03;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class ARSuite extends ApplicationAdapter {

	private SpriteBatch batch;
	private OrthographicCamera cam;
	private Texture texture,logo;

	TextureRegion textureRegion,textureRegionQuarter,textureRegionsScale,textureRegionDraw;

	int addY = 0;
	int addX = 0;
	float scaleUp = 1.5f;
	float scaleDown = 0.5f;
	float angle =0;
	float width ,height;
	int widthR = 0,heightR =0;


	@Override
	public void create () {
		Gdx.app.setLogLevel(Application.LOG_DEBUG);

		batch = new SpriteBatch();

		cam = new OrthographicCamera(Gdx.graphics.getWidth(),Gdx.graphics.getHeight());
		cam.setToOrtho(true,Gdx.graphics.getWidth(),Gdx.graphics.getHeight());

		texture = new Texture("noba.jpg");
		logo = new Texture(Gdx.files.internal("badlogic.jpg"));

		// 預先設定 TextureRegion
		textureRegion = new TextureRegion(logo, 0, 0, logo.getWidth(), logo.getHeight());
		textureRegion.flip(false,true);

		textureRegionQuarter = new TextureRegion(logo);

		textureRegionsScale = new TextureRegion(logo);
		textureRegionsScale.setRegion(0,0,logo.getWidth(),logo.getHeight());
		textureRegionsScale.flip(false,true);
		width =  textureRegionsScale.getRegionWidth()*scaleUp;
		height = textureRegionsScale.getRegionHeight()*scaleDown;


		textureRegionDraw = new TextureRegion(logo);

	}

	@Override
	public void render () {
		cam.update();

		addY+=2;
		addX+=1;
		angle = (angle+1)%360;

		batch.setProjectionMatrix(cam.combined);

		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		batch.begin();

		// 原始的渲染圖
		batch.draw(logo,0,0,logo.getWidth(),logo.getHeight(),0,0,logo.getWidth(),logo.getHeight(),false,true);

//		// 後來渲染的圖，會蓋在前面
//		batch.draw(logo, 100, 100, logo.getWidth(), logo.getHeight(), 0, 0, logo.getWidth(), logo.getHeight(), false, true);
//
//		// 使用 TextureRegion
//		batch.draw(textureRegion, 200, 200);
//
//		//繪製左下 1/4，要先設定 繪製圖片，再翻轉，才會使正常的。(在左上角)
//		textureRegionQuarter.setRegion(0,logo.getHeight()/2,logo.getWidth()/2,logo.getHeight()/2);
//		textureRegionQuarter.flip(false,true);
//		batch.draw(textureRegionQuarter,Gdx.graphics.getWidth()-logo.getHeight()/2,0);
//
//
//		// 往下移動 (後來發現 TextureRegion 比較方便操作 )
//		//batch.draw(logo,20,20+addY,logo.getWidth(),logo.getHeight(),0,0,logo.getWidth(),logo.getHeight(),false,true);
//		batch.draw(textureRegion, 300, 300+addY);
//
//		// 往右移動 (後來發現 TextureRegion 比較方便操作 )
//		//batch.draw(logo,20+addX,20,logo.getWidth(),logo.getHeight(),0,0,logo.getWidth(),logo.getHeight(),false,true);
//		batch.draw(textureRegion, 300+addX, 300);
//
//		// 往右下移動
//		batch.draw(textureRegion, 300+addX, 300+addY);
//
//		// 放大/縮小/旋轉
//		batch.draw(textureRegionsScale,400,400,0,0,width,height,scaleUp,scaleDown,angle);
//
//		// 右下角繪製
//		batch.draw(logo,Gdx.graphics.getWidth()-logo.getWidth(),Gdx.graphics.getHeight()-logo.getHeight(),logo.getWidth(),logo.getHeight(),0,0,logo.getWidth(),logo.getHeight(),false,true);
//
//		// 向右下繪出貼圖
//		if(widthR<logo.getWidth()){
//			widthR += 3;
//		}
//
//		if(heightR <logo.getHeight()){
//			heightR += 3;
//		}
//
//		textureRegionDraw.setRegion(0,0,widthR,heightR);
//		textureRegionDraw.flip(false,true);
//		batch.draw(textureRegionDraw,500,500);


		batch.end();
	}









	
	@Override
	public void dispose () {
		batch.dispose();
		texture.dispose();
	}
}
